package com.example.demo.service;

import com.example.demo.dto.ClienteDTO;
import com.example.demo.dto.TransacaoDTO;
import com.example.demo.mappers.ClienteMapper;
import com.example.demo.mappers.TransacaoMapper;
import com.example.demo.modelos.Cliente;
import com.example.demo.modelos.ClienteNaoEncontrado;
import com.example.demo.modelos.SaldoInsuficiente;
import com.example.demo.modelos.Transacao;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ClienteService {
    private Map<String , Cliente> clientes =new HashMap<>();

    public ClienteDTO getClienteByNome(String nome) {
        Cliente cliente = clientes.get(nome);
        if (cliente == null) {
            throw new ClienteNaoEncontrado("Cliente não encontrado");
        }
        return ClienteMapper.toDTO(cliente);
    }

    public TransacaoDTO criarTransacao(TransacaoDTO dto) {
        Cliente pagador = clientes.get(dto.pagador());
        Cliente recebedor = clientes.get(dto.recebedor());

        if (pagador == null || recebedor == null) {
            throw new ClienteNaoEncontrado("Pagador ou recebedor não encontrado");
        }

        if (pagador.getSaldo() < dto.quantidade()) {
            throw new SaldoInsuficiente("Saldo insuficiente");
        }

        pagador.setSaldo(pagador.getSaldo() - dto.quantidade());
        recebedor.setSaldo(recebedor.getSaldo() + dto.quantidade());

        Transacao transacao = TransacaoMapper.toEntity(dto);
        return TransacaoMapper.toDTO(transacao);
    }

    public ClienteDTO adicionarCliente(ClienteDTO dto, String senha) {
        /*if (clientes.containsKey(dto.nome())) {
            throw new RuntimeException("Cliente já existe");
        }*/
        Cliente novoCliente = new Cliente(dto.saldo(), dto.nome() , senha);
        clientes.put(novoCliente.getNome(), novoCliente);
        return ClienteMapper.toDTO(novoCliente);
    }

}
