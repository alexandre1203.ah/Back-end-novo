package com.example.demo.controller;

import com.example.demo.dto.TransacaoDTO;
import com.example.demo.service.ClienteService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transacao")
public class TransacaoController {
    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public TransacaoDTO criarTransacao(@Valid @RequestBody TransacaoDTO dto){
        return clienteService.criarTransacao(dto);
    }
}
