package com.example.demo.dto;

public record ProductRequestDTO(String name, String description, double price) {
}
