package com.example.demo.dto;

public record ProductResponseDTO(Long id, String name, String description, double price) {
}
